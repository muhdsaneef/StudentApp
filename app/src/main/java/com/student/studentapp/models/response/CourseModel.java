package com.student.studentapp.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saneef on 20/4/18.
 */

public class CourseModel {

    @SerializedName("id")
    private int courseID;

    @SerializedName("course_name")
    private String courseName;

    public CourseModel() {
    }

    public CourseModel(int courseID, String courseName) {
        this.courseID = courseID;
        this.courseName = courseName;
    }

    public int getCourseID() {
        return courseID;
    }

    public String getCourseName() {
        return courseName;
    }
}
