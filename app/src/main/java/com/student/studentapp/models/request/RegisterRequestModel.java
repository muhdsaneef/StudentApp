package com.student.studentapp.models.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saneef on 17/3/18.
 */

public class RegisterRequestModel {

    @SerializedName("user_name")
    private String username;

    private String password;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("course_id")
    private int courseID;



    public RegisterRequestModel(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }
}
