package com.student.studentapp.models.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saneef on 21/4/18.
 */

public class ExamAddRequestModel {

    @SerializedName("exam_name")
    private String examName;

    @SerializedName("exam_date")
    private String examDate;

    @SerializedName("course_type")
    private int courseType;

    public ExamAddRequestModel(String examName, String examDate, int courseType) {
        this.examName = examName;
        this.examDate = examDate;
        this.courseType = courseType;
    }
}
