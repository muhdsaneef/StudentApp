package com.student.studentapp.models.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saneef on 17/3/18.
 */

public class LoginRequestModel {

    @SerializedName("user_name")
    private String username;

    private String password;

    public LoginRequestModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
