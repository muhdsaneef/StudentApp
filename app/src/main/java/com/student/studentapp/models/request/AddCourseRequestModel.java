package com.student.studentapp.models.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saneef on 21/4/18.
 */

public class AddCourseRequestModel {

    @SerializedName("course_name")
    private String courseName;

    public AddCourseRequestModel(String courseName) {
        this.courseName = courseName;
    }
}
