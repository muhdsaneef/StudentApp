package com.student.studentapp.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saneef on 30/3/18.
 */

public class UserModel {

    private String id;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("user_name")
    private String userName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("is_admin")
    private boolean isAdmin;

    @SerializedName("course_type")
    private int courseID;

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getUserName() {
        return userName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public int getCourseID() {
        return courseID;
    }
}
