package com.student.studentapp.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saneef on 31/3/18.
 */

public class ExamModel {

    @SerializedName("exam_name")
    private String examName;

    @SerializedName("exam_date")
    private String examTime;

    @SerializedName("course_type")
    private String courseType;

    public String getExamName() {
        return examName;
    }

    public String getExamTime() {
        return examTime;
    }

    public String getCourseType() {
        return courseType;
    }
}
