package com.student.studentapp.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by saneef on 20/4/18.
 */

public class CourseListResponseModel extends BaseResponseModel {

    @SerializedName("data")
    private List<CourseModel> examList;

    public List<CourseModel> getExamList() {
        return examList;
    }
}
