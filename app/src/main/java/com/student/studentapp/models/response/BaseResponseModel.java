package com.student.studentapp.models.response;

import com.google.gson.annotations.SerializedName;
import com.student.studentapp.utils.Constants;

/**
 * Created by saneef on 17/3/18.
 */

public class BaseResponseModel {

    private String status;

    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message != null ? message : "";
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess(){
        return status != null && status.equalsIgnoreCase(Constants.API_STATUS_SUCCESS);
    }
}
