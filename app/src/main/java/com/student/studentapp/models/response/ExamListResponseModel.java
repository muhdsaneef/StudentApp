package com.student.studentapp.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by saneef on 31/3/18.
 */

public class ExamListResponseModel extends BaseResponseModel {

    @SerializedName("data")
    private List<ExamModel> examList;

    public List<ExamModel> getExamList() {
        return examList;
    }
}
