package com.student.studentapp.interfaces;

/**
 * Created by saneef on 21/4/18.
 */

public interface CourseSelectedListener {

    void onCourseSelected(int courseId, String courseName);
}
