package com.student.studentapp.interfaces;

import com.student.studentapp.models.response.CourseModel;

import java.util.List;

/**
 * Created by saneef on 20/4/18.
 */

public interface CourseApiListener {

    void onCourseListFetched(List<CourseModel> coursesList);
    void onApiFailure(String errorMessaage);
}
