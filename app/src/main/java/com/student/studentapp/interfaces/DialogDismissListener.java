package com.student.studentapp.interfaces;

/**
 * Created by saneef on 17/3/18.
 */

public interface DialogDismissListener {

    void onDialogDismissed();
}
