package com.student.studentapp.utils;

/**
 * Created by saneef on 17/3/18.
 */

public class Constants {

    public static final String API_STATUS_SUCCESS = "success";
    public static final String KEY_FIRST_NAME = "User's first name";
    public static final String KEY_USER_COURSE_TYPE = "User's course type";
    public static final String KEY_ADMIN_USER = "Is user an admin?";
    public static final String PDF = ".pdf";
    public static final Object FOLDER_NAME = "/StudentApp";
    public static final int REQUEST_WRITE_PERMISSION = 9001;
}
