package com.student.studentapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by saneef on 17/3/18.
 */

public class LocalStorage {

    private static LocalStorage mLocalStorage;
    private SharedPreferences mSharedPreferences;
    private static final String LOCAL_STORAGE = "Student App storage";
    private static final String USER_SESSION = "User session";


    private LocalStorage(Context context){
        mSharedPreferences = context.getSharedPreferences(LOCAL_STORAGE, Context.MODE_PRIVATE);
    }


    public static LocalStorage getStorageManager(Context context){
        if(mLocalStorage == null) {
            mLocalStorage = new LocalStorage(context);
        }
        return mLocalStorage;
    }

    public void saveUserSession() {
        mSharedPreferences.edit().putBoolean(USER_SESSION, true).apply();
    }

    public void clearUserSession(){
        mSharedPreferences.edit().putBoolean(USER_SESSION, false).apply();
    }

    public boolean readUserSession() {
        return mSharedPreferences.getBoolean(USER_SESSION, false);
    }

    public void writeStringToLocalStorage(String key, String value) {
        mSharedPreferences.edit().putString(key, value).apply();
    }

    public String readStringFromLocalStorage(String key){
       return mSharedPreferences.getString(key, "");
    }

    public void writeBooleanToLocalStorage(String keyAdminUser, boolean value) {
        mSharedPreferences.edit().putBoolean(keyAdminUser, value).apply();
    }

    public boolean readBoolean(String key){
        return mSharedPreferences.getBoolean(key, false);
    }

    public int readIntegerFromLocalStorage(String key) {
        return mSharedPreferences.getInt(key, -1);
    }

    public void writeInteger(String key, int value) {
        mSharedPreferences.edit().putInt(key, value).apply();
    }
}
