package com.student.studentapp.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.student.studentapp.R;
import com.student.studentapp.adapters.ExamListAdapter;
import com.student.studentapp.api.ApiManager;
import com.student.studentapp.interfaces.CourseSelectedListener;
import com.student.studentapp.models.response.BaseResponseModel;
import com.student.studentapp.models.response.ExamListResponseModel;
import com.student.studentapp.models.response.ExamModel;
import com.student.studentapp.utils.AppUtilities;
import com.student.studentapp.utils.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends BaseActivity {

    private TextView tvGreetings;
    private TextView tvNoExams;
    private Button btnLogout;
    private RecyclerView rcvExamList;
    private Button btnDownloadSyllabus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initViews();
        setClickListeners();
        setUserGreetings();
        getUpcomingExamsList();
    }

    private void getUpcomingExamsList() {
        Call<ExamListResponseModel> call = ApiManager.getApiManager().getExamsList(getUserCourseType());
        if(AppUtilities.isInternetAvailable(this)){
            showProgressDialog();
            call.enqueue(new Callback<ExamListResponseModel>() {
                @Override
                public void onResponse(Call<ExamListResponseModel> call, Response<ExamListResponseModel> response) {
                    hideProgressDialog();
                    if(response.isSuccessful()){
                        if(response.body().isSuccess()){
                            showExamList(response.body().getExamList());

                        } else {
                            showToast(response.body().getMessage());
                        }
                    } else {
                        showToast(getString(R.string.error_api_failure));
                    }
                }

                @Override
                public void onFailure(Call<ExamListResponseModel> call, Throwable t) {
                    hideProgressDialog();
                    showToast(getString(R.string.error_api_failure));
                }
            });
        } else {
            showToast(getString(R.string.error_no_network));
        }
    }

    private void showExamList(List<ExamModel> upcomingExamsList) {
        boolean isExamsListEmpty = upcomingExamsList.isEmpty();
        tvNoExams.setVisibility(isExamsListEmpty ? View.VISIBLE : View.GONE);
        rcvExamList.setVisibility(isExamsListEmpty ? View.GONE : View.VISIBLE);
        if(!upcomingExamsList.isEmpty()) {
            setRecyclerView(upcomingExamsList);
        }
    }

    private void setRecyclerView(List<ExamModel> upcomingExamsList) {
        ExamListAdapter adapter = new ExamListAdapter(this, upcomingExamsList);
        rcvExamList.setLayoutManager(new LinearLayoutManager(this));
        rcvExamList.setAdapter(adapter);
    }

    private void setUserGreetings() {
        String userGreetings = getString(R.string.greetings)+ getUserFirstName();
        tvGreetings.setText(userGreetings);
    }

    private void setClickListeners() {
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearUserSession();
                showLoginScreen();
            }
        });

        btnDownloadSyllabus.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(checkForWritePermission()){
                    downloadSyllabus();
                } else {
                    requestWritePermission();
                }

            }
        });
    }

    private void downloadSyllabus() {
        Call<ResponseBody> call = ApiManager.getApiManager().downloadSyllabus(getUserCourseType());
        if(AppUtilities.isInternetAvailable(this)){
            showProgressDialog();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        writeToFile(getSyllabusFileName(), response.body());
                    } else {
                        hideProgressDialog();
                        showToast(getString(R.string.error_api_failure));
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideProgressDialog();
                    showToast(getString(R.string.error_api_failure));
                }
            });
        } else {
            showToast(getString(R.string.error_no_network));
        }
    }

    private void initViews() {
        tvGreetings = (TextView) findViewById(R.id.greetings);
        tvNoExams = (TextView) findViewById(R.id.tv_no_exams);
        btnLogout = (Button) findViewById(R.id.btn_logout);
        btnDownloadSyllabus = (Button) findViewById(R.id.btn_download_syllabus);
        rcvExamList = (RecyclerView) findViewById(R.id.rcv_upcoming_exams);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == Constants.REQUEST_WRITE_PERMISSION) {
            if(!checkForWritePermission()) {
                showStorageAccessRequiredMessage();
            } else {
                showToast(getString(R.string.press_download_again));
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showStorageAccessRequiredMessage() {
        showToast(getString(R.string.storage_access_required));
    }

    private String getSyllabusFileName() {
        String fileName = "Syllabus_";
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
        String fileNameDate = dateFormat.format(currentDate);
        return fileName + fileNameDate + Constants.PDF;
    }

    private boolean checkForWritePermission() {
        String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int result = checkCallingOrSelfPermission(permission);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestWritePermission() {
        requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_WRITE_PERMISSION);
    }

    private void writeToFile(String fileName, ResponseBody responseBody){

        InputStream inputStream;
        FileOutputStream outputStream;
        File dir = new File(Environment.getExternalStorageDirectory().toString() + Constants.FOLDER_NAME);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        File syllabusFile = new File(dir, fileName);
        try {
            syllabusFile.createNewFile();
        } catch (IOException e) {
            Log.e("File creation", e.toString());
        }

        try {

            byte[] fileReader = new byte[4096];

            long fileSize = responseBody.contentLength();
            long fileSizeDownloaded = 0;

            inputStream = responseBody.byteStream();
            outputStream = new FileOutputStream(syllabusFile);

            while (true) {
                int read = inputStream.read(fileReader);

                if (read == -1) {
                    break;
                }

                outputStream.write(fileReader, 0, read);

                fileSizeDownloaded += read;

                Log.d("StudentApp", "file download: " + fileSizeDownloaded + " of " + fileSize);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();
            showMessageDialog(getString(R.string.file_stored_at)+ syllabusFile.getAbsolutePath(), null);
            hideProgressDialog();

        } catch (Exception ex) {
            hideProgressDialog();
            Log.d("File error", ex.toString());
        }
    }

}
