package com.student.studentapp.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.student.studentapp.R;
import com.student.studentapp.api.ApiManager;
import com.student.studentapp.interfaces.CourseSelectedListener;
import com.student.studentapp.models.response.BaseResponseModel;
import com.student.studentapp.models.request.AddCourseRequestModel;
import com.student.studentapp.models.request.ExamAddRequestModel;
import com.student.studentapp.utils.AppUtilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminActivity extends BaseActivity {

    private static final int REQUEST_SYLLABUS_FILE = 9000;
    private static final int SECTION_EXAM = 1;
    private static final int SECTION_SYLLABUS = 2;
    private EditText edtExamName;
    private TextView edtExamDate;
    private EditText edtCourseName;
    private TextView edtCourseType;
    private TextView edtSyllabusFile;
    private Button btnAddExam;
    private Button btnAddCourse;
    private Button btnAddSyllabus;
    private Button btnLogout;
    private TextView edtCourseID;
    private File syllabusFile;
    private CourseSelectedListener listener;
    private int courseID;
    private int currentSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        initVariables();
        initViews();
        setClickListener();
        initCourseListOptions();
        initCourseSelectionListener();
        setListener(listener);

    }

    private void initVariables() {
        courseID = -1;
        currentSection = -1;
    }

    private void initCourseSelectionListener() {
        listener = new CourseSelectedListener() {
            @Override
            public void onCourseSelected(int courseId, String courseName) {
                courseID = courseId;
                if(currentSection == SECTION_EXAM){
                    edtCourseID.setText(courseName);
                } else if(currentSection == SECTION_SYLLABUS){
                    edtCourseType.setText(courseName);
                }
            }
        };
    }

    private void setClickListener() {
        btnAddExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateExamFields()){
                    addNewExam();
                } else {
                    showToast(getString(R.string.required_fields_empty));
                }
            }
        });

        btnAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateCourseField()){
                    addNewCourse();
                } else {
                    showToast(getString(R.string.cours_name_empty));
                }

            }
        });

        btnAddSyllabus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUploadFields()) {
                    uploadSyllabus();
                } else {
                    showToast(getString(R.string.required_fields_empty));
                }
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearUserSession();
                showLoginScreen();
            }
        });

        edtSyllabusFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                chooseFileFromStorage();
            }
        });

        edtCourseID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                currentSection = SECTION_EXAM;
                showCourses(v);
            }
        });

        edtCourseType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                currentSection = SECTION_SYLLABUS;
                showCourses(v);
            }
        });

        edtExamDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                showDatePcker();
            }
        });


    }

    private void showDatePcker() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                setSelectedDate(year, month, dayOfMonth);
            }
        };
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(this, listener, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setSelectedDate(int year, int month, int dayOfMonth) {
        //This format is required to match with backend date field
        String dateFormat = "yyyy-MM-dd";
        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String formattedDate = dateFormatter.format(calendar.getTime());
        edtExamDate.setText(formattedDate);
    }

    private boolean validateUploadFields() {
        String courseID = edtCourseType.getText().toString().trim();
        String fileName = edtSyllabusFile.getText().toString().trim();
        return !courseID.isEmpty() && !fileName.isEmpty();

    }

    private boolean validateCourseField() {
        return !edtCourseName.getText().toString().trim().isEmpty();
    }

    private boolean validateExamFields() {
        String examName = edtExamName.getText().toString().trim();
        String examDate = edtExamDate.getText().toString().trim();
        String courseID = edtCourseID.getText().toString().trim();
        return !examName.isEmpty() && !examDate.isEmpty() && !courseID.isEmpty();
    }

    private void initViews() {
        edtExamName = (EditText) findViewById(R.id.edt_exam_title);
        edtExamDate = (TextView) findViewById(R.id.edt_exam_date);
        edtCourseName = (EditText) findViewById(R.id.edt_course_name);
        edtCourseType = (TextView) findViewById(R.id.edt_course_type);
        edtCourseID = (TextView) findViewById(R.id.edt_course_id);
        edtSyllabusFile = (TextView) findViewById(R.id.edt_syllabus_file);

        btnAddExam = (Button) findViewById(R.id.btn_add_exam);
        btnAddCourse = (Button) findViewById(R.id.btn_add_course);
        btnAddSyllabus = (Button) findViewById(R.id.btn_upload_syllabus);
        btnLogout = (Button) findViewById(R.id.btn_logout);
    }

    private void addNewExam(){
        Call<BaseResponseModel> call = ApiManager.getApiManager().createExam(createAddExamRequest());
        if(AppUtilities.isInternetAvailable(this)){
            showProgressDialog();
            call.enqueue(new Callback<BaseResponseModel>() {
                @Override
                public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                    hideProgressDialog();
                    if(response.isSuccessful()){
                        BaseResponseModel responseBody = response.body();
                        if(responseBody.isSuccess()){
                            showToast(getString(R.string.exam_added));
                        } else {
                            showToast(responseBody.getMessage());
                        }
                    } else {
                        showToast(getString(R.string.error_api_failure));
                    }
                }

                @Override
                public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                    hideProgressDialog();
                    showToast(getString(R.string.error_api_failure));
                }
            });
        } else {
            showToast(getString(R.string.error_no_network));
        }

    }

    private ExamAddRequestModel createAddExamRequest() {
        String examName = edtExamName.getText().toString().trim();
        String examDate = edtExamDate.getText().toString().trim();
        return new ExamAddRequestModel(examName, examDate, courseID);
    }

    private void addNewCourse(){
        Call<BaseResponseModel> call = ApiManager.getApiManager().createCourse(createAddCourseRequest());
        if(AppUtilities.isInternetAvailable(this)){
            showProgressDialog();
            call.enqueue(new Callback<BaseResponseModel>() {
                @Override
                public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                    hideProgressDialog();
                    if(response.isSuccessful()){
                        BaseResponseModel responseBody = response.body();
                        if(responseBody.isSuccess()){
                            showToast(getString(R.string.course_added));
                        } else {
                            showToast(responseBody.getMessage());
                        }
                    } else {
                        showToast(getString(R.string.error_api_failure));
                    }
                }

                @Override
                public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                    hideProgressDialog();
                    showToast(getString(R.string.error_api_failure));
                }
            });
        } else {
            showToast(getString(R.string.error_no_network));
        }
    }

    private AddCourseRequestModel createAddCourseRequest() {
        String courseName = edtCourseName.getText().toString().trim();
        return new AddCourseRequestModel(courseName);
    }

    private void uploadSyllabus(){
        MediaType mediaType = MediaType.parse("multipart/form-data");
        RequestBody courseIDValue = RequestBody.create(mediaType, String.valueOf(courseID));
        RequestBody syllabusFileBody = RequestBody.create(MediaType.parse("application/pdf"), syllabusFile);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("syllabus_file", "syllabusFile.pdf", syllabusFileBody);
        Call<BaseResponseModel> call = ApiManager.getApiManager().uploadSyllabus(courseIDValue, body);
        if(AppUtilities.isInternetAvailable(this)){
            showProgressDialog();
            call.enqueue(new Callback<BaseResponseModel>() {
                @Override
                public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                    hideProgressDialog();
                    if(response.isSuccessful()){
                        BaseResponseModel responseBody = response.body();
                        if(responseBody.isSuccess()){
                            showToast(getString(R.string.syllabus_file_updated));
                        } else {
                            showToast(responseBody.getMessage());
                        }
                    } else {
                        showToast(getString(R.string.error_api_failure));
                    }
                }

                @Override
                public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                    hideProgressDialog();
                    showToast(getString(R.string.error_api_failure));
                }
            });
        } else {
            showToast(getString(R.string.error_no_network));
        }
    }

    private void chooseFileFromStorage(){
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), REQUEST_SYLLABUS_FILE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_SYLLABUS_FILE && resultCode == RESULT_OK){
            showProgressDialog();
            Uri fileUri = data.getData();
            handleChosenFile(fileUri);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleChosenFile(Uri fileUri) {
        boolean fileObtained = getActualFile(fileUri);
        if(fileObtained){
            showToast(getString(R.string.file_read_success));
            edtSyllabusFile.setText(fileUri.getLastPathSegment());
        } else {
            showToast(getString(R.string.file_read_error));
        }
    }

    public boolean getActualFile(Uri uri){
        InputStream inputStream;
        try {
            inputStream = getContentResolver().openInputStream(uri);
            //Convert stream to data here
            File file = new File(getExternalFilesDir(null), "tempFile.pdf");
            OutputStream outputStream =
                    new FileOutputStream(file);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            inputStream.close();
            syllabusFile = file;
            outputStream.close();
            hideProgressDialog();
            return true;

        } catch (FileNotFoundException e) {
            Log.e("File conversion error", e.toString());
           return false;
        } catch (IOException e) {
            Log.e("File conversion error", e.toString());
            return false;
        }
    }


}
