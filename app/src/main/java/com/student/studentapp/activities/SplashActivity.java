package com.student.studentapp.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.student.studentapp.R;
import com.student.studentapp.activities.DashboardActivity;
import com.student.studentapp.activities.LoginActivity;
import com.student.studentapp.utils.Constants;
import com.student.studentapp.utils.LocalStorage;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Check if user session is present
        boolean isUserLoggedIn = LocalStorage.getStorageManager(this).readUserSession();
        boolean isUserAdmin = LocalStorage.getStorageManager(this).readBoolean(Constants.KEY_ADMIN_USER);
        //Decide which screen to launch based user session
        Class activityClass;
        if(isUserLoggedIn){
            activityClass = isUserAdmin ? AdminActivity.class : DashboardActivity.class;
        } else {
            activityClass = LoginActivity.class;
        }
        //Launch decided screen with 2 seconds delay
        navigateWithDelay(activityClass);

    }

    private void navigateWithDelay(final Class activityClass) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                launchActivity(activityClass);
            }
        }, SPLASH_DELAY);
    }

    /**
     * Start a new activity
     * @param activityClass The class of activity which is to be started
     */
    private void launchActivity(Class activityClass) {
        Intent intent = new Intent(this, activityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
