package com.student.studentapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.student.studentapp.R;
import com.student.studentapp.api.ApiManager;
import com.student.studentapp.models.request.LoginRequestModel;
import com.student.studentapp.models.response.LoginResponseModel;
import com.student.studentapp.models.response.UserModel;
import com.student.studentapp.utils.AppUtilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener{

    private EditText edtUsername;
    private EditText edtPassword;
    private TextView tvRegisterHere;
    private Button btnLogin;

    private static final String NOT_REGISTERED = "Not registered?";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();

        setClickListeners();
        setTouchListeners();
        setupRegisterLink();
    }

    private ClickableSpan clickableSpan = new ClickableSpan() {
        @Override
        public void onClick(View widget) {
            showRegistrationScreen();
        }
    };

    private void showRegistrationScreen() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void setupRegisterLink() {
        String registerHereText = tvRegisterHere.getText().toString();
        SpannableString spannableString = new SpannableString(registerHereText);
        spannableString.setSpan(clickableSpan, NOT_REGISTERED.length() + 1,
                                             registerHereText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black))
                ,NOT_REGISTERED.length() + 1, registerHereText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvRegisterHere.setText(spannableString);
        tvRegisterHere.setMovementMethod(new LinkMovementMethod());
    }

    private void setTouchListeners() {
        edtUsername.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    edtUsername.setError(null);
                }
                return false;
            }
        });

        edtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    edtPassword.setError(null);
                }
                return false;
            }
        });
    }

    private void setClickListeners() {
        btnLogin.setOnClickListener(this);
    }

    private void initViews() {
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        tvRegisterHere = (TextView) findViewById(R.id.tv_register_link);
        btnLogin = (Button) findViewById(R.id.btn_login);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.btn_login:
                if(validateInputs()){
                    performLogin();
                }
                break;
        }
    }

    private void performLogin() {
        if(AppUtilities.isInternetAvailable(this)) {
            showProgressDialog();
            Call<LoginResponseModel> call = ApiManager.getApiManager().performLogin(createLoginRequest());
            call.enqueue(new Callback<LoginResponseModel>() {
                @Override
                public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                    hideProgressDialog();
                    if(response.isSuccessful()) {
                        if(response.body().isSuccess()){
                            saveUserSession(response.body().getData());
                            redirectUser(response.body().getData());
                        } else {
                            showToast(response.body().getMessage());
                        }
                    } else {
                        showToast(getString(R.string.error_api_failure));
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                    hideProgressDialog();
                    showToast(getString(R.string.error_api_failure));
                }
            });
        } else {
            showToast(getString(R.string.error_no_network));
        }
    }


    private void redirectUser(UserModel data) {
        Intent intent;
        if(data.getIsAdmin()){
            intent = new Intent(this, AdminActivity.class);
        } else {
            intent = new Intent(this, DashboardActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private LoginRequestModel createLoginRequest() {
        String username = edtUsername.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        return new LoginRequestModel(username, password);
    }

    private boolean validateInputs() {
        boolean isValid = true;
        if(edtPassword.getText().toString().trim().isEmpty()){
            edtPassword.setError(getString(R.string.error_required_field));
            edtPassword.requestFocus();
            isValid = false;
        }
        if(edtUsername.getText().toString().trim().isEmpty()){
            edtUsername.setError(getString(R.string.error_required_field));
            edtUsername.requestFocus();
            isValid = false;
        }
        return isValid;
    }
}
