package com.student.studentapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListPopupWindow;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.student.studentapp.R;
import com.student.studentapp.adapters.CourseAdapter;
import com.student.studentapp.api.CourseApiManager;
import com.student.studentapp.interfaces.CourseApiListener;
import com.student.studentapp.interfaces.CourseSelectedListener;
import com.student.studentapp.interfaces.DialogDismissListener;
import com.student.studentapp.models.response.CourseModel;
import com.student.studentapp.models.response.UserModel;
import com.student.studentapp.utils.Constants;
import com.student.studentapp.utils.LocalStorage;
import com.student.studentapp.widgets.MessageDialog;

import java.util.List;

public class BaseActivity extends AppCompatActivity implements CourseApiListener, CourseSelectedListener {

    private ProgressDialog progressDialog;
    private View rootView;
    private ListPopupWindow listPopupWindow;
    private CourseAdapter adapter;
    private CourseModel selectedCourse;
    private CourseSelectedListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initRootView();
        initProgressDialog();
    }

    public void initCourseListOptions(){
        listPopupWindow = new ListPopupWindow(this);
        getCoursesList();
    }

    private void getCoursesList() {
        CourseApiManager.getCoursesList(this, this);
        showProgressDialog();
    }

    private void initRootView() {
        rootView = getWindow().getDecorView().getRootView();

        if(rootView != null){
            rootView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP){
                        hideKeyboard(v);
                    }
                    return true;
                }
            });
        }
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.please_wait));
    }

    public void setListener(CourseSelectedListener listener) {
        this.listener = listener;
    }

    public void showProgressDialog() {
        if(progressDialog != null && !progressDialog.isShowing()){
            progressDialog.show();
        }
    }

    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    public void hideKeyboard(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showMessageDialog(String message, DialogDismissListener listener) {
        MessageDialog messageDialog = new MessageDialog();
        messageDialog.setContext(this);
        messageDialog.setDismissListener(listener);
        messageDialog.setDisplayMessage(message);
        messageDialog.show(getFragmentManager(), MessageDialog.TAG);
    }

    protected void clearUserSession() {
        LocalStorage.getStorageManager(this).clearUserSession();
        LocalStorage.getStorageManager(this).writeStringToLocalStorage(Constants.KEY_FIRST_NAME, "");
        LocalStorage.getStorageManager(this).writeBooleanToLocalStorage(Constants.KEY_ADMIN_USER, false);
    }

    public void saveUserSession(UserModel user) {
        LocalStorage.getStorageManager(this).saveUserSession();
        LocalStorage.getStorageManager(this).writeStringToLocalStorage(Constants.KEY_FIRST_NAME, user.getFirstName());
        LocalStorage.getStorageManager(this).writeBooleanToLocalStorage(Constants.KEY_ADMIN_USER, user.getIsAdmin());
        LocalStorage.getStorageManager(this).writeInteger(Constants.KEY_USER_COURSE_TYPE, user.getCourseID());
    }

    public String getUserFirstName(){
        return LocalStorage.getStorageManager(this).readStringFromLocalStorage(Constants.KEY_FIRST_NAME);
    }

    public int getUserCourseType() {
        return LocalStorage.getStorageManager(this).readIntegerFromLocalStorage(Constants.KEY_USER_COURSE_TYPE);
    }

    protected void showLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void showCourses(View anchorView){
        if(listPopupWindow != null && adapter != null){
            listPopupWindow.setAnchorView(anchorView);
            listPopupWindow.setWidth(anchorView.getWidth());
            listPopupWindow.setModal(true);
            listPopupWindow.show();
        }
    }

    @Override
    public void onCourseListFetched(List<CourseModel> coursesList) {
        hideProgressDialog();
        setupCoursesAdapter(coursesList);
        setAdapterToList();
    }

    public CourseModel getSelectedCourse() {
        return selectedCourse;
    }

    private void setAdapterToList() {
        listPopupWindow.setAdapter(adapter);
    }

    private void setupCoursesAdapter(List<CourseModel> coursesList) {
        adapter = new CourseAdapter(this, R.layout.item_course, coursesList);
        adapter.setListener(this);
    }

    @Override
    public void onApiFailure(String errorMessaage) {
        hideProgressDialog();
        showToast(getString(R.string.error_api_failure));
    }

    @Override
    public void onCourseSelected(int courseId, String courseName) {
        selectedCourse = new CourseModel(courseId, courseName);
        listPopupWindow.dismiss();
        if(listener != null){
            listener.onCourseSelected(courseId, courseName);
        }
    }
}
