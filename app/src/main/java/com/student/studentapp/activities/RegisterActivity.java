package com.student.studentapp.activities;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.student.studentapp.R;
import com.student.studentapp.api.ApiManager;
import com.student.studentapp.interfaces.CourseSelectedListener;
import com.student.studentapp.interfaces.DialogDismissListener;
import com.student.studentapp.models.response.BaseResponseModel;
import com.student.studentapp.models.request.RegisterRequestModel;
import com.student.studentapp.utils.AppUtilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity implements DialogDismissListener{

    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtUserName;
    private EditText edtPassword;
    private Button btnRegister;
    private TextView edtCourseType;
    private CourseSelectedListener listener;
    private int courseID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();

        setClickListeners();
        setTouchListeners();
        initCourseListOptions();
        initCourseSelectionListener();
        setListener(listener);

    }

    private void initCourseSelectionListener() {
        listener = new CourseSelectedListener() {
            @Override
            public void onCourseSelected(int courseId, String courseName) {
                courseID = courseId;
                edtCourseType.setText(courseName);
            }
        };
    }

    private void setTouchListeners() {
        edtFirstName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    edtFirstName.setError(null);
                }
                return false;
            }
        });
        edtLastName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    edtLastName.setError(null);
                }
                return false;
            }
        });
        edtUserName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    edtUserName.setError(null);
                }
                return false;
            }
        });
        edtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    edtPassword.setError(null);
                }
                return false;
            }
        });
    }

    private void setClickListeners() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateInputFields()){
                    performUserRegistration();
                }
            }
        });

        edtCourseType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtCourseType.setError(null);
                hideKeyboard(v);
                showCourses(v);
            }
        });
    }

    private boolean validateInputFields() {
        boolean isValid = true;
        if(edtCourseType.getText().toString().trim().isEmpty()){
            edtCourseType.setError(getString(R.string.error_field_required));
            edtCourseType.requestFocus();
            isValid = false;
        }
        if(edtPassword.getText().toString().trim().isEmpty()){
            edtPassword.setError(getString(R.string.error_field_required));
            edtPassword.requestFocus();
            isValid = false;
        }
        if(edtUserName.getText().toString().trim().isEmpty()){
            edtUserName.setError(getString(R.string.error_field_required));
            edtUserName.requestFocus();
            isValid = false;
        }
        if(edtLastName.getText().toString().trim().isEmpty()){
            edtLastName.setError(getString(R.string.error_field_required));
            edtLastName.requestFocus();
            isValid = false;
        }
        if(edtFirstName.getText().toString().trim().isEmpty()){
            edtFirstName.setError(getString(R.string.error_field_required));
            edtFirstName.requestFocus();
            isValid = false;
        }
        return isValid;
    }

    private void performUserRegistration() {
        if(AppUtilities.isInternetAvailable(this)){
            showProgressDialog();
            Call<BaseResponseModel> call = ApiManager.getApiManager().registerStudent(createRegisterRequest());
            call.enqueue(new Callback<BaseResponseModel>() {
                @Override
                public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                    hideProgressDialog();
                    if(response.isSuccessful()) {
                        if(response.body().isSuccess()){
                            showMessageDialog(getString(R.string.message_registeration_success), RegisterActivity.this);
                        } else {
                            showToast(response.body().getMessage());
                        }
                    } else {
                        showToast(getString(R.string.error_api_failure));
                    }
                }

                @Override
                public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                    hideProgressDialog();
                    showToast(getString(R.string.error_api_failure));
                }
            });

        } else {
            showToast(getString(R.string.error_no_network));
        }
    }

    private RegisterRequestModel createRegisterRequest() {
        String username = edtUserName.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        String firstName = edtFirstName.getText().toString().trim();
        String lastName = edtLastName.getText().toString().trim();
        RegisterRequestModel request = new RegisterRequestModel(username, password, firstName, lastName);
        request.setCourseID(courseID);
        return request;
    }

    private void initViews() {
        edtFirstName = (EditText) findViewById(R.id.edt_first_name);
        edtLastName = (EditText) findViewById(R.id.edt_last_name);
        edtUserName = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtCourseType = (TextView) findViewById(R.id.edt_course_type);
        btnRegister = (Button) findViewById(R.id.btn_register);
    }

    @Override
    public void onDialogDismissed() {
        finish();
    }
}