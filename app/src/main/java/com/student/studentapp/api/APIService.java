package com.student.studentapp.api;

import com.student.studentapp.models.response.BaseResponseModel;
import com.student.studentapp.models.response.CourseListResponseModel;
import com.student.studentapp.models.response.ExamListResponseModel;
import com.student.studentapp.models.request.LoginRequestModel;
import com.student.studentapp.models.response.LoginResponseModel;
import com.student.studentapp.models.request.AddCourseRequestModel;
import com.student.studentapp.models.request.ExamAddRequestModel;
import com.student.studentapp.models.request.RegisterRequestModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by saneef on 17/3/18.
 */

public interface APIService {

    @POST("login/")
    Call<LoginResponseModel> performLogin(@Body LoginRequestModel request);

    @POST("add_user/")
    Call<BaseResponseModel> registerStudent(@Body RegisterRequestModel request);

    @GET("exam_list/")
    Call<ExamListResponseModel> getExamsList(@Query("course_type") int courseType);

    @GET("course_list/")
    Call<CourseListResponseModel> getCourseList();

    @POST("add_exam/")
    Call<BaseResponseModel> createExam(@Body ExamAddRequestModel request);

    @POST("add_course/")
    Call<BaseResponseModel> createCourse(@Body AddCourseRequestModel request);

    @Multipart
    @POST("upload_syllabus/")
    Call<BaseResponseModel> uploadSyllabus(@Part("course_id")RequestBody courseID, @Part MultipartBody.Part syllabusFile);

    @GET("get_syllabus/")
    Call<ResponseBody> downloadSyllabus(@Query("course_id") int courseID);

}
