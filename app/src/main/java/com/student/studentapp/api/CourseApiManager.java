package com.student.studentapp.api;

import android.content.Context;

import com.student.studentapp.R;
import com.student.studentapp.interfaces.CourseApiListener;
import com.student.studentapp.models.response.CourseListResponseModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by saneef on 20/4/18.
 */

public class CourseApiManager {

    public static void getCoursesList(final Context context, final CourseApiListener listener){
        Call<CourseListResponseModel> apiCall = ApiManager.getApiManager().getCourseList();
        apiCall.enqueue(new Callback<CourseListResponseModel>() {
            @Override
            public void onResponse(Call<CourseListResponseModel> call, Response<CourseListResponseModel> response) {
                if(response.isSuccessful()){
                    CourseListResponseModel responseBody = response.body();
                    if(responseBody.isSuccess()){
                        if(listener != null){
                            listener.onCourseListFetched(responseBody.getExamList());
                        }
                    } else {
                        listener.onApiFailure(responseBody.getMessage());
                    }
                } else {
                    listener.onApiFailure(context.getString(R.string.error_api_failure));
                }
            }

            @Override
            public void onFailure(Call<CourseListResponseModel> call, Throwable t) {
                if(t instanceof IOException) {
                    listener.onApiFailure(context.getString(R.string.error_no_network));
                }else {
                    listener.onApiFailure(context.getString(R.string.error_api_failure));
                }
            }
        });
    }
}
