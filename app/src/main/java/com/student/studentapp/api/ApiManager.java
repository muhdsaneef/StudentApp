package com.student.studentapp.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by saneef on 17/3/18.
 */

public class ApiManager {

    private static ApiManager mApiManager;

    private static final String API_BASE_URL = "https://backendforandroid.herokuapp.com/user/api/v1/";
    private Retrofit mRetrofit;
    private APIService mApiService;

    private ApiManager() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        mRetrofit = new Retrofit.Builder()
                                .baseUrl(API_BASE_URL)
                                .client(httpClient)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
        mApiService = mRetrofit.create(APIService.class);
    }

    public static APIService getApiManager(){
        if(mApiManager == null) {
            mApiManager = new ApiManager();
        }
        return mApiManager.mApiService;
    }

}
