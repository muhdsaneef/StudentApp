package com.student.studentapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.student.studentapp.R;
import com.student.studentapp.models.response.ExamModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by saneef on 31/3/18.
 */

public class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.ViewHolder> {

    private List<ExamModel> mExamList;
    private Context context;

    public ExamListAdapter(Context context, List<ExamModel> mExamList) {
        this.mExamList = mExamList;
        this.context = context;
    }

    @Override
    public ExamListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_upcoming_exam, null);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ExamListAdapter.ViewHolder holder, int position) {
        String examText = "Exam name: " + mExamList.get(position).getExamName();
        holder.tvExamName.setText(examText);
        String examDateText = "Exam date: " + getFormattedDate(mExamList.get(position).getExamTime());
        holder.tvExamDate.setText(examDateText);
    }

    private String getFormattedDate(String date) {
        String currentFormat = "yyyy-MM-dd'T'HH:MM:ssZ";
        String requiredFormat = "dd-MM-yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(currentFormat, Locale.getDefault());
        String formattedDate = "";
        try {
            Date dateObj = dateFormat.parse(date.replace("Z", "GMT+00:00"));
            dateFormat.applyPattern(requiredFormat);
            formattedDate = dateFormat.format(dateObj);
        } catch (ParseException e) {
            Log.e("Date parser", e.toString());
        }
        return formattedDate;
    }

    @Override
    public int getItemCount() {
        return mExamList != null ? mExamList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvExamName;
        private TextView tvExamDate;

        public ViewHolder(View itemView) {
            super(itemView);
            initViews(itemView);
            ViewGroup.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);
        }

        private void initViews(View itemView) {
            tvExamName = (TextView) itemView.findViewById(R.id.tv_exam_name);
            tvExamDate = (TextView) itemView.findViewById(R.id.tv_exam_date_time);
        }
    }
}
