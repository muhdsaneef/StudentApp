package com.student.studentapp.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.student.studentapp.R;
import com.student.studentapp.interfaces.CourseSelectedListener;
import com.student.studentapp.models.response.CourseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saneef on 21/4/18.
 */

public class CourseAdapter extends ArrayAdapter<CourseModel> {
    private List<CourseModel> listOfCourses;
    private CourseSelectedListener listener;


    public CourseAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CourseModel> objects) {
        super(context, resource, objects);
        listOfCourses = new ArrayList<>();
        listOfCourses.addAll(objects);
    }

    @Override
    public int getCount() {
        return listOfCourses != null ? listOfCourses.size() : 0;
    }

    public void setListener(CourseSelectedListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View itemView = convertView;
        if(itemView == null){
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_course, null);
        }
        TextView tvCourseName = (TextView)itemView.findViewById(R.id.tv_course_name);
        tvCourseName.setText(listOfCourses.get(position).getCourseName());

        setClickListener(itemView, position);
        return itemView;
    }

    private void setClickListener(View itemView, final int position) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    int courseID = listOfCourses.get(position).getCourseID();
                    String courseName = listOfCourses.get(position).getCourseName();
                    listener.onCourseSelected(courseID, courseName);
                }
            }
        });
    }
}
