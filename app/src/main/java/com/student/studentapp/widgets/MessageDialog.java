package com.student.studentapp.widgets;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.student.studentapp.R;
import com.student.studentapp.interfaces.DialogDismissListener;

/**
 * Created by saneef on 17/3/18.
 */

public class MessageDialog extends DialogFragment {

    public static final String TAG = "MessageDialog";
    private Context context;
    private DialogDismissListener dismissListener;
    private TextView tvMessage;
    private Button btnOk;
    private String mDisplayMessage;


    public void setContext(Context context) {
        this.context = context;
    }

    public DialogDismissListener getDismissListener() {
        return dismissListener;
    }

    public void setDismissListener(DialogDismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    public void setDisplayMessage(String mDisplayMessage) {
        this.mDisplayMessage = mDisplayMessage;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.message_dialog, null);
        initDialogViews(rootView);

        if(getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setClickListeners();
        return rootView;
    }

    private void setClickListeners() {
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dismissListener != null){
                    dismissListener.onDialogDismissed();
                }
                dismiss();
            }
        });
    }

    private void initDialogViews(View dialogView) {
        tvMessage = (TextView) dialogView.findViewById(R.id.tv_message);
        btnOk = (Button) dialogView.findViewById(R.id.btn_ok);

        if(mDisplayMessage != null){
            tvMessage.setText(mDisplayMessage);
        }
    }
}
